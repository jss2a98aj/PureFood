package codeberg.unix_supremacist.purefood;

import codeberg.unix_supremacist.purefood.api.item.ItemEnum;
import codeberg.unix_supremacist.purefood.api.item.Items;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class Recipe {
    public static void load(){
        GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(Items.metaItem, 1, ItemEnum.DOUGH.ordinal()), new Object[]{net.minecraft.init.Items.wheat, "toolMortar"}));
    }
}

package codeberg.unix_supremacist.purefood.api.item;

import codeberg.unix_supremacist.purefood.CommonProxy;
import codeberg.unix_supremacist.purefood.api.item.instance.SeedFoodInstance;
import codeberg.unix_supremacist.purefood.api.item.interfaces.IItemPlantable;
import codeberg.unix_supremacist.purefood.api.item.interfaces.IMetaItem;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lombok.Getter;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSeedFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import java.util.HashMap;
import java.util.List;

public class MetaItem extends ItemSeedFood implements IMetaItem, IItemPlantable {
	@Getter final String modid;
	public static final HashMap<Integer, SeedFoodInstance> items = new HashMap<>();
	public static final HashMap<Integer, Boolean> itemsMeat = new HashMap<>();
	@SideOnly(Side.CLIENT)
	IIcon[] icons;

	public MetaItem(String modid, Block farmland) {
		super( 0, 0, null, farmland);
		this.modid = modid;
		this.setHasSubtypes(true);
		this.setCreativeTab(CommonProxy.pftab);
	}

	public ItemStack addItem(String name, int meta, Block crop, int hunger, float saturation, boolean isMeat) {
		this.items.put(meta, new SeedFoodInstance(name, crop, hunger, saturation));
		this.itemsMeat.put(meta, isMeat);
		return new ItemStack(this, 1, meta);
	}

	protected SeedFoodInstance getItem(ItemStack item) {
		return this.items.get(item.getItemDamage());
	}

	/**
	 * Get hunger
	 *
	 * @param item food being checked
	 * @return the hunger restored
	 */
	@Override
	public int func_150905_g(ItemStack item) {
		return this.getItem(item).getHunger();
	}

	/**
	 * Get saturation
	 *
	 * @param item food being checked
	 * @return the saturation float
	 */
	@Override
	public float func_150906_h(ItemStack item) {
		return this.getItem(item).getSaturation();
	}

	@Override
	public String getUnlocalizedName(ItemStack item) {
		return this.getUnlocalizedName(this.getModid(), this.getItem(item).getName());
	}

	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List itemList) {
		this.getSubItems(this, this.items, itemList);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister register) {
		this.registerIcons(this.getModid(), this.items, register);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int meta) {
		return this.getIconFromDamage(this.items, meta);
	}

	@Override
	public boolean onItemUse(ItemStack item, EntityPlayer player, World world, int x, int y, int z, int u, float u1, float u2, float u3) {
		if (this.getItem(item).getCrop() != null){
			return this.onItemUse(this, item, player, world, x, y, z, u, u1, u2, u3, this.getItem(item).getCrop());
		}
		return false;
	}

	@Override
	public EnumAction getItemUseAction(ItemStack item) {
		if(this.getItem(item).getHunger() != 0 || this.getItem(item).getHunger() != 0){
			return EnumAction.eat;
		}
		return EnumAction.none;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack item, World world, EntityPlayer player) {
		if(this.getItem(item).getHunger() != 0 || this.getItem(item).getHunger() != 0){
			if (player.canEat(false)){ //should be always edible fix later
				player.setItemInUse(item, this.getMaxItemUseDuration(item));
			}
		}
		return item;
	}

	public static boolean isMeat(int meta){
		return itemsMeat.get(meta);
	}
}
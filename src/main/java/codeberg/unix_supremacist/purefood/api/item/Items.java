package codeberg.unix_supremacist.purefood.api.item;

import codeberg.unix_supremacist.purefood.DynamicConfigs;
import codeberg.unix_supremacist.purefood.Tags;
import codeberg.unix_supremacist.purefood.api.block.CropBase;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Items {
    public static MetaItem metaItem;
    public static Item mortar = new ToolItem(256);
    public static void load() {
        //Create items
        metaItem = new MetaItem(Tags.MODID, Blocks.farmland);
        //Registers them to the forge registry
        GameRegistry.registerItem(metaItem, "metaItem");
        GameRegistry.registerItem(mortar, "mortar");
        //Iterates items
        for (ItemEnum item : ItemEnum.values()) {
            if(DynamicConfigs.getToggled(item.name(), true)) addItem(
                    Tags.MODID, metaItem, item.name().toLowerCase(), item.ordinal(),
                    DynamicConfigs.getHunger(item.name(), item.hunger),
                    DynamicConfigs.getSaturation(item.name(), item.saturation),
                    item.crop, item.meat
            );
        }
    }

    public static ItemStack addItem(String MODID, MetaItem item, String name, int meta, int hunger, float saturation, boolean hasCrop, boolean isMeat){
        if (hasCrop){
            CropBase crop = new CropBase(MODID, name, item, meta);
            GameRegistry.registerBlock(crop, name);
            return item.addItem(name, meta, crop, hunger, saturation, isMeat);
        } else {
            return item.addItem(name, meta, null, hunger, saturation, isMeat);
        }
    }
}
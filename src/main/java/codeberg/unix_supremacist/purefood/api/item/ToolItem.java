package codeberg.unix_supremacist.purefood.api.item;

import codeberg.unix_supremacist.purefood.CommonProxy;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ToolItem extends Item {
    public ToolItem(int damage){
        this.maxStackSize = 1;
        this.setMaxDamage(damage);
        this.setCreativeTab(CommonProxy.pftab);
    }

    public boolean hasContainerItem(ItemStack stack) {
        return this.getDamage(stack) != this.getMaxDamage();
    }

    public ItemStack getContainerItem(ItemStack stack) {
        if (!hasContainerItem(stack)) return null;
        //stack.attemptDamageItem(stack.getItemDamage()+1, ); this should be used with a random call instead for enchantments
        stack.setItemDamage(stack.getItemDamage()+1);
        return stack;
    }

    public boolean doesContainerItemLeaveCraftingGrid(ItemStack p_77630_1_){
        return false;
    }
}

package codeberg.unix_supremacist.purefood.api.item;

public enum ItemEnum {
    ANANAS(false, 4, 0.3f),
    APPLE_CORE(false, 0, 0.0f),
    BACON_AND_EGGS(false), //todo
    BAGUETTE(false, 8, 1.2f),
    BAGUETTES(false, 8, 1.2f),
    BANANA(false, 1, 0.6f),
    BAR_BUTTER(false, 1, 4.0f),
    BAR_CHEESE(false, 2, 1.2f),
    BAR_CHOCOLATE(false, 4, 0.8f),
    BAR_SOYLENT(false), //todo
    BAR_TOFU(false), //todo
    BARK_CINNAMON(false), //todo
    BBQ_RIBS(false, 10, 1.6f, true),
    BEANS(false),
    BEANS_MUNG(false), //todo
    BEANS_RED(false), //todo
    BEANS_URAD(false), //todo
    BEET(false),
    BERRY_BILL(false, 1, 0.6f),
    BERRY_BLACK(false, 1, 0.6f),
    BERRY_BLUE(false, 1, 0.6f),
    BERRY_CANDLE(false, 1, 0.6f),
    BERRY_CRAN(false, 1, 0.6f),
    BERRY_FISHER(false, 1, 0.6f),
    BERRY_GOOSE(false, 1, 0.6f),
    BERRY_MUL(false, 1, 0.6f),
    BERRY_PRICKLE(false, 1, 0.6f),
    BERRY_RASP(false, 1, 0.6f),
    BERRY_STRAW(false, 1, 0.6f),
    BERRY_SUN(false, 1, 0.6f),
    BIG_BREAD(false), //todo
    BOILED_EGG(false, 2, 1.2f),
    BREADS(false, 5, 1.2f),
    BROTH_BONE(false), //todo
    BRYONY(false), //todo
    BUN(false, 2, 1.2f),
    BUNS(false, 2, 1.2f),
    BURGER(false, 6, 1.6f),
    BURGER_CHEESE(false, 4, 1.4f),
    BURGER_CHUM(false, 6, 1.6f),
    BURGER_FISH(false, 6, 1.6f),
    BURGER_MEAT(false), //todo
    BURGER_SOYLENT(false), //todo
    BURGER_TOFU(false), //todo
    BURGER_VEGGIE(false), //todo
    CABBAGE(false), //todo
    CAKE_BOTTOM(false, 3, 0.2f),
    CAKE_CHEESE_SWEET_BERRY(false), //todo
    CHEESE(false), //todo
    CHEESE_AGED(false), //todo
    CHICKPEAS(false), //todo
    CHIPS_CHILI(false), //todo
    CHIPS(false), //todo
    CHUM(false), //todo
    COCONUT(false), //todo
    COOKED_BACON(false), //todo
    COOKED_CHEVAL(false), //todo
    COOKED_CHICKEN_WINGS(false), //todo
    COOKED_CHOPS_MUTTON(false), //todo
    COOKED_DOGMEAT(false), //todo
    COOKED_HAM(false), //todo
    COOKED_HORSE_MEAT(false), //todo
    COOKED_MUTTON(false, 7, 2.0f, true),
    COOKED_MYSTERYMEAT(false), //todo
    COOKED_PATTY_BEEF(false), //todo
    COOKED_RIB_EYE_STEAK(false), //todo
    COOKED_RIBS(false), //todo
    COOKED_RICE(false), //todo
    COOKED_SAUSAGE(false), //todo
    COOKED_SLAB_BEEF(false), //todo
    COOKED_SLAB_CHEVAL(false), //todo
    COOKED_SLAB_MOOSHROOM(false), //todo
    COOKED_SLAB_PORK(false), //todo
    COOKED_SLICE_HAM(false), //todo
    COOKIE_CHOCOLATE_RAISIN(false), //todo
    COOKiE_RAISIN(false), //todo
    COOKIE_SWEET_BERRY(false), //todo
    CORNKERNELS(false), //todo
    CREAM_CHOCOLATE(false), //todo
    CREAM_COCONUT(false), //todo
    CREAM_HEAVY(false), //todo
    CUCUMBER(false), //todo
    CURDS_CHEESE(false), //todo
    CURRANTS_BLACK(false), //todo
    CURRANTS_RED(false), //todo
    CURRANTS_WHITE(false), //todo
    DOG_FOOD(false), //todo
    DOGMEAT(false), //todo
    DOUGH(false), //todo
    DOUGH_ABYSSAL(false, 1, 1.0f),
    DOUGH_CHOCOLATE(false), //todo
    DOUGH_COOKIE(false), //todo
    DOUGH_COOKIE_ABYSSAL(false, 1, 0.2f),
    DOUGH_COOKIE_CHOCOLATE_RAISIN(false), //todo
    DOUGH_COOKIE_RAISIN(false), //todo
    DOUGH_LOAF(false), //todo
    DOUGH_PASTA(false), //todo
    DOUGH_PASTA_FLAT(false), //todo
    DOUGH_PIZZA_TOMATO_SAUCED(false), //todo
    DOUGH_SUGAR(false), //todo
    DOUGH_SUGAR_CHOCOLATE_RAISIN(false), //todo
    DOUGH_SUGAR_RAISIN(false), //todo
    DRESSING(false), //todo
    DRIED_JERKY(false), //todo
    DRIED_RAISINS_CHOCOLATE(false), //todo
    DRIED_RAISINS_GREEN(false), //todo
    DRIED_RAISINS_POMEGRANATE(false), //todo
    DRIED_RAISINS_PURPLE(false), //todo
    DRIED_RAISINS_RED(false), //todo
    DRIED_RAISINS_WHITE(false), //todo
    DUMPLINGS(false), //todo
    EGG_SCRAMBLED(false), //todo
    EGG_WHITE(false), //todo
    EGG_YOLK(false), //todo
    FIG(false), //todo
    FRIED_EGG(false), //todo
    FRIED_RICE(false), //todo
    FRIES(false), //todo
    GARLIC(false), //todo
    GLOW_BERRY_CUSTARD(false), //todo
    GRAIN_BARLEY(true),
    GRAIN_RICE(true),
    GRAIN_RYE(false),
    GRAPES_GREEN(false), //todo
    GRAPES_PURPLE(false), //todo
    GRAPES_RED(false), //todo
    GRAPES_WHITE(false), //todo
    GRILLED_SALMON(false), //todo
    HARDTACK(false), //todo
    HAZELNUT(false), //todo
    HONEY(false), //todo
    HONEY_GLAZED_HAM(false), //todo
    HONEYDEW(false), //todo
    ICE_CREAM(false), //todo
    ICE_CREAM_ANANAS(false), //todo
    ICE_CREAM_APPLE(false), //todo
    ICE_CREAM_BACON(false), //todo
    ICE_CREAM_BANANA(false), //todo
    ICE_CREAM_BERRY_BLACK(false), //todo
    ICE_CREAM_BERRY_BLUE(false), //todo
    ICE_CREAM_BERRY_CRAN(false), //todo
    ICE_CREAM_BERRY_GOOSE(false), //todo
    ICE_CREAM_BERRY_RASP(false), //todo
    ICE_CREAM_BERRY_STRAW(false), //todo
    ICE_CREAM_CARAMEL(false), //todo
    ICE_CREAM_CHERRY(false), //todo
    ICE_CREAM_CHOCOLATE_CHIP(false), //todo
    ICE_CREAM_CHOCOLATE(false), //todo
    ICE_CREAM_CHUM(false), //todo
    ICE_CREAM_CURRANT(false), //todo
    ICE_CREAM_GRAPE(false), //todo
    ICE_CREAM_HONEY(false), //todo
    ICE_CREAM_KIWI(false), //todo
    ICE_CREAM_LEMON(false), //todo
    ICE_CREAM_MAPLE(false), //todo
    ICE_CREAM_MELON(false), //todo
    ICE_CREAM_MINT_CHOCOLATE_CHIP(false), //todo
    ICE_CREAM_MINT(false), //todo
    ICE_CREAM_MOCHA(false), //todo
    ICE_CREAM_NEAPOLITAN(false), //todo
    ICE_CREAM_NUTELLA(false), //todo
    ICE_CREAM_PEANUT_BUTTER(false), //todo
    ICE_CREAM_PISTACHIO(false), //todo
    ICE_CREAM_RAISIN(false), //todo
    ICE_CREAM_SPUMONI_CHOCOLATE(false), //todo
    ICE_CREAM_SPUMONI_VANILLA(false), //todo
    ICE_CREAM_VANILLA(false), //todo
    LEAF_CABBAGE(false), //todo
    LEEK(false), //todo
    LEMON(false), //todo
    LEMONADE(false), //todo
    LENTILS(false), //todo
    LETTUCE(false), //todo
    LIMONCELLO(false), //todo
    LOAF(false), //todo
    MASH_HOPS(false), //todo
    MASH_WHEAT_HOPS(false), //todo
    MASH_WHEAT(false), //todo
    MILK(false), //todo
    MILK_COCONUT(false), //todo
    MILK_SOY(false), //todo
    MUCKROOT(false), //todo
    MUSHROOM_RICE(false), //todo
    NOODLES_VEGETABLE(false), //todo
    OATS(false), //todo
    OIL_FISH(false), //todo
    OIL_HEMP(false), //todo
    OIL_LIN(false), //todo
    OIL_NUT(false), //todo
    OIL_OLIVE(false), //todo
    OIL_SEED(false), //todo
    OIL_SUNFLOWER(false), //todo
    OIL_WHALE(false), //todo
    PANTAO(false), //todo
    PASTA_MEATBALLS(false), //todo
    PASTA_MUTTON_CHOP(false), //todo
    PASTA_SQUID_INK(false), //todo
    PEANUT(false), //todo
    PEPPER(false), //todo
    PIE_APPLE(false), //todo
    PIE_CHOCOLATE(false), //todo
    PIE_CRUST(false), //todo
    PIE_SHEPHERDS(false), //todo
    PILE_RICE(false), //todo
    PIZZA_DOUGH(false), //todo
    PIZZA_MARGHERITA(false), //todo
    PIZZA_MINCE_MEAT(false), //todo
    PIZZA_PINEAPPLE_HAM(false), //todo
    PIZZA_VEGGIE(false), //todo
    POMEGRANATE(false), //todo
    RADISH(false), //todo
    RATATOUILLE(false), //todo
    RAW_BACON(false), //todo
    RAW_BAGUETTE(false), //todo
    RAW_BREAD(false), //todo
    RAW_BUN(false), //todo
    RAW_CAKE_BOTTOM(false), //todo
    RAW_CHEVAL(false), //todo
    RAW_CHICKEN_WINGS(false), //todo
    RAW_CHIPS(false), //todo
    RAW_CHOPS_MUTTON(false), //todo
    RAW_HAM(false), //todo
    RAW_HORSE_MEAT(false), //todo
    RAW_MARGHERITA_PIZZA(false), //todo
    RAW_MINCE_MEAT_PIZZA(false), //todo
    RAW_MUTTON(false), //todo
    RAW_MYSTERYMEAT(false), //todo
    RAW_PASTA(false), //todo
    RAW_PATTY_BEEF(false), //todo
    RAW_PINEAPPLE_HAM_PIZZA(false), //todo
    RAW_RIB_EYE_STEAK(false), //todo
    RAW_RIBS(false), //todo
    RAW_SAUSAGE(false), //todo
    RAW_SLAB_BEEF(false), //todo
    RAW_SLAB_CHEVAL(false), //todo
    RAW_SLAB_MOOSHROOM(false), //todo
    RAW_SLAB_PORK(false), //todo
    RAW_VEGGIE_PIZZA(false), //todo
    RED_ONION(false), //todo
    ROAST_CHICKEN(false), //todo
    ROAST_CHOPS_MUTTON(false), //todo
    ROLL_CABBAGE(false), //todo
    ROLL_COD(false), //todo
    ROLL_KELP(false), //todo
    ROLL_SALMON(false), //todo
    ROTTEN_TOMATO(false), //todo
    SALAD_FRUIT(false), //todo
    SALAD_MIXED(false), //todo
    SALAD_NETHER(false), //todo
    SALTED_BAR_BUTTER(false), //todo
    SALTED_SLAB_BEEF(false), //todo
    SALTED_SLAB_CHEVAL(false), //todo
    SALTED_SLAB_PORK(false), //todo
    SANDWICH_BACON(false), //todo
    SANDWICH_CHEESE(false), //todo
    SANDWICH_CHICKEN(false), //todo
    SANDWICH_EGG(false), //todo
    SANDWICH_LARGE_BACON(false), //todo
    SANDWICH_LARGE_CHEESE(false), //todo
    SANDWICH_LARGE_STEAK(false), //todo
    SANDWICH_LARGE_VEGGIE(false), //todo
    SANDWICH_STEAK(false), //todo
    SANDWICH_VEGGIE(false), //todo
    SAUCE_BBQ(false), //todo
    SAUCE_CHILI(false), //todo
    SAUCE_HOT(false), //todo
    SAUCE_KETCHUP(false), //todo
    SAUCE_MAPLE_SAP(false), //todo
    SAUCE_MAPLE_SYRUP(false), //todo
    SAUCE_MAYO(false), //todo
    SAUCE_PEANUT_BUTTER(false), //todo
    SAUCE_ROYAL_JELLY(false), //todo
    SAUCE_TOMATO(false), //todo
    SCRAP_MEAT(false), //todo
    SILVER_PEAR(false), //todo
    SLICE_ANANAS(false), //todo
    SLICE_APPLE(false), //todo
    SLICE_BAGUETTE(false), //todo
    SLICE_BANANA(false), //todo
    SLICE_BREAD(false), //todo
    SLICE_BUN(false), //todo
    SLICE_CAKE(false), //todo
    SLICE_CAKE_CHEESE_SWEET_BERRY(false), //todo
    SLICE_CANTALOUPE(false), //todo
    SLICE_CARROT(false), //todo
    SLICE_CHEESE(false), //todo
    SLICE_COCONUT(false), //todo
    SLICE_COOKED_COD(false), //todo
    SLICE_COOKED_FISH(false), //todo
    SLICE_COOKED_SALMON(false), //todo
    SLICE_CUCUMBER(false), //todo
    SLICE_EGG(false), //todo
    SLICE_EGGPLANT(false), //todo
    SLICE_HONEYDEW(false), //todo
    SLICE_HORNEDMELON(false), //todo
    SLICE_LEMON(false), //todo
    SLICE_LOAF(false), //todo
    SLICE_ONION(false), //todo
    SLICE_PIE_APPLE(false), //todo
    SLICE_PIE_CHOCOLATE(false), //todo
    SLICE_PIE_PUMPKIN(false), //todo
    SLICE_PUMPKIN(false), //todo
    SLICE_RAW_COD(false), //todo
    SLICE_RAW_FISH(false), //todo
    SLICE_RAW_HAM(false), //todo
    SLICE_RAW_SALMON(false), //todo
    SLICE_ROLL_KELP(false), //todo
    SLICE_SMOKED_FISH(false), //todo
    SLICE_SMOKED_SALMON(false), //todo
    SLICE_SQUASH(false), //todo
    SLICE_TOMATO(false), //todo
    SLICE_WINTERMELON(false), //todo
    SLIME_BOTTLE_BLUE(false), //todo
    SLIME_BOTTLE_GREEN(false), //todo
    SLIME_BOTTLE_PINK(false), //todo
    SOUP_CHICKEN(false), //todo
    SOUP_NOODLE(false), //todo
    SOUP_PUMPKIN(false), //todo
    SOUP_VEGETABLE(false), //todo
    SPICES(false), //todo
    SPINACH(false), //todo
    STEAK_AND_POTATOES(false), //todo
    STEW_BAKED_COD(false), //todo
    STEW_BEEF(false), //todo
    STEW_FISH(false), //todo
    STEW_SWEETPOD(false), //todo
    STRIPS_POTATO(false), //todo
    STUFFED_POTATO(false), //todo
    STUFFED_PUMPKIN(false), //todo
    SWEETPOD(false), //todo
    TOAST(false), //todo
    TOMATILLOS(false), //todo
    TOMATO(false), //todo
    TURKISH_DELIGHT(false), //todo
    VINEGAR_CANE(false), //todo
    VINEGAR_CIDER(false), //todo
    VINEGAR_GRAPE(false), //todo
    VINEGAR_RICE(false), //todo
    WHEEL_AGED_CHEESE(false), //todo
    WHEEL_CHEESE(false), //todo
    WHITE_ONION(false), //todo
    WHEEL_WAXED_CHEESE(false), //todo
    WISDOMFRUIT(false), //todo
    WRAP_MUTTON(false); //todo

    boolean crop;
    int hunger;
    float saturation;
    boolean meat;

    ItemEnum(boolean crop, int hunger, float saturation, boolean meat) {
        this.crop = crop;
        this.hunger = hunger;
        this.saturation = saturation;
        this.meat = meat;
    }

    ItemEnum(boolean crop, int hunger, float saturation) {
        this.crop = crop;
        this.hunger = hunger;
        this.saturation = saturation;
        this.meat = false;
    }

    ItemEnum(boolean crop) {
        this(crop, 0, 0);
    }
}

package codeberg.unix_supremacist.purefood.api.item.instance;

import lombok.Getter;

public class FoodInstance extends ItemInstance {
	@Getter final int hunger;
	@Getter final float saturation;

	public FoodInstance(String name, int healing, float saturation) {
		super(name);
		this.hunger = healing;
		this.saturation = saturation;
	}
}

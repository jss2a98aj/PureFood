package codeberg.unix_supremacist.purefood.api.item.instance;

import lombok.Getter;
import net.minecraft.block.Block;

public class SeedFoodInstance extends FoodInstance{
	@Getter final Block crop;
	@Getter final boolean wolf;

	public SeedFoodInstance(String name, Block crop, int healing, float saturation, boolean wolf) {
		super(name, healing, saturation);
		this.crop = crop;
		this.wolf = wolf;
	}

	public SeedFoodInstance(String name, Block crop, int healing, float saturation) {
		this(name, crop, healing, saturation, false);
	}
}
package codeberg.unix_supremacist.purefood.api.item.instance;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.IIcon;

public class ItemInstance {
	@Getter final String name;
	@Getter @Setter IIcon icon;

	public ItemInstance(String name) {
		this.name = name;
	}
}
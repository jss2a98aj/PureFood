package codeberg.unix_supremacist.purefood;

import codeberg.unix_supremacist.purefood.api.item.ItemEnum;
import codeberg.unix_supremacist.purefood.api.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class Oredict {
    public static void load(){
        OreDictionary.registerOre("toolMortarandpestle", new ItemStack(Items.mortar, 1, OreDictionary.WILDCARD_VALUE));
        fastRegister("listAllfruit", ItemEnum.ANANAS.ordinal());
        fastRegister("itemPlantRemains", ItemEnum.APPLE_CORE.ordinal());
        fastRegister("listAllfruit", ItemEnum.BANANA.ordinal());
        fastRegister("foodButter", ItemEnum.BAR_BUTTER.ordinal());
        fastRegister("foodCheese", ItemEnum.BAR_CHEESE.ordinal());
        fastRegister("foodChocolatebar", ItemEnum.BAR_CHOCOLATE.ordinal());
        fastRegister("listAllmeatraw", ItemEnum.BAR_SOYLENT.ordinal());
        fastRegister("listAllmeatraw", ItemEnum.BAR_TOFU.ordinal());
        fastRegister("foodRibbbq", ItemEnum.BBQ_RIBS.ordinal());
        fastRegister("listAllveggie", ItemEnum.BEANS.ordinal());
        fastRegister("listAllveggie", ItemEnum.BEANS_MUNG.ordinal());
        fastRegister("listAllveggie", ItemEnum.BEANS_RED.ordinal());
        fastRegister("listAllveggie", ItemEnum.BEANS_URAD.ordinal());
    }

    public static void fastRegister(String od, int meta){
        if(new ItemStack(Items.metaItem, 1, meta).getUnlocalizedName() != null){
            OreDictionary.registerOre(od, new ItemStack(Items.metaItem, 1, meta));
        }
    }
}

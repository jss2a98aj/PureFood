package codeberg.unix_supremacist.purefood;

import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.common.config.Configuration;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class DynamicConfigs {
    public static final String catGeneral = "general";
    public static final String catHunger = "hunger";
    public static final String catSaturation = "saturation";

    private static Configuration contentTogglesConfig;
    private static Configuration foodValuesConfig;

    static {
        String configFolder =  "config" + File.separator + Tags.MODID + File.separator;

        contentTogglesConfig = new Configuration(new File(Launch.minecraftHome, configFolder + "contentToggles.cfg"));
        foodValuesConfig = new Configuration(new File(Launch.minecraftHome, configFolder + "foodValues.cfg"));
    }

    public static boolean getToggled(String contentName, boolean defaultEnabled) {
        if(contentTogglesConfig != null) {
            String nameLower = contentName.toLowerCase();
            return contentTogglesConfig.getBoolean(
                    "enable_" + nameLower,
                    catGeneral, defaultEnabled,
                    "Enables " + StringUtils.capitalize(nameLower.replace("_", " "))
            );
        } else {
            return defaultEnabled;
        }
    }

    public static int getHunger(String itemName, int defaultHunger) {
        if(foodValuesConfig != null) {
            String nameLower = itemName.toLowerCase();
            return foodValuesConfig.getInt(
                    nameLower + "_hunger", catHunger,
                    defaultHunger, 0, 20,
                    StringUtils.capitalize(nameLower.replace("_", " ")) + " hunger value."
            );
        } else {
            return defaultHunger;
        }
    }

    public static float getSaturation(String itemName, float defaultSaturation) {
        if(foodValuesConfig != null) {
            String nameLower = itemName.toLowerCase();
            return foodValuesConfig.getFloat(
                    nameLower + "_saturation", catSaturation,
                    defaultSaturation, 0f, 20f,
                    StringUtils.capitalize(nameLower.replace("_", " ")) + " saturation value."
            );
        } else {
            return defaultSaturation;
        }
    }

    public static void commitConfigs() {
        if(contentTogglesConfig != null && contentTogglesConfig.hasChanged()) {
            contentTogglesConfig.save();
        }

        if(foodValuesConfig != null && foodValuesConfig.hasChanged()) {
            foodValuesConfig.save();
        }

        //Let the GC clean up because we do not need these after init.
        contentTogglesConfig = null;
        foodValuesConfig = null;
    }

}

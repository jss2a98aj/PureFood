package codeberg.unix_supremacist.purefood;

import codeberg.unix_supremacist.purefood.api.item.Items;
import codeberg.unix_supremacist.purefood.api.item.MetaItem;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.EntityInteractEvent;

public class EventMangler {
    @SubscribeEvent
    public void onEntityInteraction(EntityInteractEvent event) {
        if (!(event.target instanceof EntityWolf)) return;
        if (!((EntityWolf)event.target).isTamed()) return;
        if (event.entityPlayer.getHeldItem() == null || event.entityPlayer.getHeldItem().getItem() != Items.metaItem) return;

        int damage = event.entityPlayer.getHeldItem().getItemDamage();
        if (!MetaItem.isMeat(damage)) return;
        if (((EntityWolf)event.target).getHealth() >= ((EntityWolf)event.target).getMaxHealth()) {
            //breed
        } else {
            ((EntityWolf)event.target).heal(MetaItem.items.get(damage).getHunger());
            if (!event.entityPlayer.capabilities.isCreativeMode)
                if (event.entityPlayer.getHeldItem().stackSize-- <= 0)
                    event.entityPlayer.inventory.setInventorySlotContents(event.entityPlayer.inventory.currentItem, (ItemStack)null);
        }
        event.setCanceled(true);
    }
}